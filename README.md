# vue-labyrinth

https://meisterk.codeberg.page/vue-labyrinth/

## DFS

Mit DFS (Depth First Search = Tiefensuche) sucht die App einen Weg durch ein Labyrinth.

## BFS

Mit BFS (Breadth First Search = Breitensuche) suche die App den kürzesten Weg durch ein Labyrinth.